<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use DB;
use DataTables;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = School::latest()->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                   $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->school_id.'" data-original-title="Edit" class="edit btn btn-warning btn-sm editSchool">Update</a>';
                   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->school_id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteSchool">Delete</a>';

                    return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
      
        return view('schools');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        School::updateOrCreate(['school_id' => $request->school_id], [
            'school_name' => $request->get('school_name'),
            'address' => $request->get('address'),
            'telephone' => $request->get('telephone'),
            'email' => $request->get('email'),
            'website' => $request->get('website'),
            'male_students' => $request->get('male_students'),
            'female_students' => $request->get('female_students'),
            'total_students' => $request->get('total_students'),
            'regular_faculty' => $request->get('regular_faculty'),
            'parttime_faculty' => $request->get('parttime_faculty'),
            'total_faculty' => $request->get('total_faculty')]);        
   
        return response()->json(['success'=>'School saved successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($school_id)
    {
        $school = School::find($school_id);
        return response()->json($school);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($school_id)
    {
        School::find($school_id)->delete();
        return response()->json(['success'=>'School deleted successfully.']);
    }
}
