<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $primaryKey = 'school_id';
    protected $fillable = [
    	'school_name',
    	'address',
    	'telephone',
    	'email',
    	'website',
    	'male_students',
    	'female_students',
    	'total_students',
    	'regular_faculty',
    	'parttime_faculty',
    	'total_faculty'];
}
