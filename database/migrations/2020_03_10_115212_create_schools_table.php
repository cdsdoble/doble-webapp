<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('school_id');
            $table->string('school_name');
            $table->string('address');
            $table->string('telephone');
            $table->string('email');
            $table->string('website');
            $table->string('male_students');
            $table->string('female_students');
            $table->string('total_students');
            $table->string('regular_faculty');
            $table->string('parttime_faculty');
            $table->string('total_faculty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
