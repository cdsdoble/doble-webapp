<!DOCTYPE html>
<html>
<head>
    <title>Web App Project</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>


    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--datatables-->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>

<style>
  body {
    background-color: #ddd;
  }

  .card {
    padding: 25px;
  }

</style>


<body>
<div class="container">
    <br><br>
    <a class="btn btn-info" href="javascript:void(0)" id="createNewSchool"> Create New School</a>
    <br><br>
    <div class="card">
    <table class="table data-table">
        <thead class="thead-light">
            <tr>
                <th>School</th>
                <th>Address</th>
                <th>Telephone Num</th>
                <th width="150px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
   
<div class="modal fade" id="schModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="SchoolForm" name="SchoolForm" class="form-horizontal">
                   <input type="hidden" name="school_id" id="school_id">
                   <div class="row">
                      <div class="col-md-12">
                        <label for="name" class="control-label">Name</label>
                        <input type="text" class="form-control schlfrm" id="school_name" name="school_name" value="" required=""><br>
                      </div>

                    <div class="col-md-12">
                        <label for="address" class="control-label">Address</label>
                        <input type="text" class="form-control schlfrm" id="address" name="address" value="" required=""><br>
                    </div>
                    </div>
                        
                    <div class="row">
                        <div class="col-md-4">
                            <label for="telephone" class="control-label">Telephone</label>
                            <input type="text" class="form-control schlfrm" id="telephone" name="telephone" value="" maxlength="50" required=""><br>
                        </div>
                        <div class="col-md-4">
                            <label for="email" class="control-label">E-mail</label>
                            <input type="email" class="form-control schlfrm" id="email" name="email" value="" maxlength="50" required=""><br>
                        </div>
                        <div class="col-md-4">
                            <label for="website" class="control-label">Website</label>
                            <input type="text" class="form-control schlfrm" id="website" name="website" value="" maxlength="50" required=""><br>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label for="male_students" class="control-label">Male Students</label>
                            <input type="number" class="form-control schlfrm" id="male_students" name="male_students" value="" maxlength="50" min="0" required=""><br>
                        </div>
                        <div class="col-md-4">
                            <label for="female_students" class="control-label">Female Students</label>
                            <input type="number" class="form-control schlfrm" id="female_students" name="female_students" value="" maxlength="50" min="0" required=""><br>
                        </div>
                        <div class="col-md-4">
                            <label for="total_students" class="control-label">Total Students</label>
                            <input type="number" class="form-control schlfrm" id="total_students" name="total_students" value="" maxlength="50" min="0" required="" readonly><br>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label for="regular_faculty" class="control-label">Regular Faculty</label>
                            <input type="text" class="form-control schlfrm" id="regular_faculty" name="regular_faculty" value="" maxlength="50" required=""><br>
                        </div>
                        <div class="col-md-4">
                            <label for="parttime_faculty" class="control-label">Parttime Faculty</label>
                            <input type="text" class="form-control schlfrm" id="parttime_faculty" name="parttime_faculty" value="" maxlength="50" required=""><br>
                        </div>
                        <div class="col-md-4">
                            <label for="total_faculty" class="control-label">Total Faculty</label>
                            <input type="text" class="form-control schlfrm" id="total_faculty" name="total_faculty" value="" maxlength="50" required="" readonly><br>
                        </div>
                    </div>

                    <p id="demo"></p>
      
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create" onclick="formValidation()">Save
                     </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function formValidation() {
      var x = document.getElementsById("school_name").required;
      document.getElementById("demo").innerHTML = x;
    }
</script>

<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('schools.index') }}",
        columns: [
            {data: 'school_name', school_name: 'school_name'},
            {data: 'address', name: 'address'},
            {data: 'telephone', name: 'telephone'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewSchool').click(function () {
        $('#saveBtn').val("create-school");
        $('#school_id').val('');
        $('#SchoolForm').trigger("reset");
        $('#modelHeading').html("Create New School");
        $('#schModal').modal('show');
    });
    
    $('body').on('click', '.editSchool', function () {
      var school_id = $(this).data('id');
      $.get("{{ route('schools.index') }}" +'/' + school_id +'/edit', function (data) {
          $('#modelHeading').html("Edit School");
          $('#saveBtn').val("edit-school");
          $('#schModal').modal('show');
          $('#school_id').val(data.school_id);
          $('#school_name').val(data.school_name);
          $('#address').val(data.address);
          $('#telephone').val(data.telephone);
          $('#email').val(data.email);
          $('#website').val(data.website);
          $('#male_students').val(data.male_students);
          $('#female_students').val(data.female_students);
          $('#total_students').val(data.total_students);
          $('#regular_faculty').val(data.regular_faculty);
          $('#parttime_faculty').val(data.parttime_faculty);
          $('#total_faculty').val(data.total_faculty);
      })
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
    
        $.ajax({
          data: $('#SchoolForm').serialize(),
          url: "{{ route('schools.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#SchoolForm').trigger("reset");
              $('#schModal').modal('hide');
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });
    
    $('body').on('click', '.deleteSchool', function () {
        var school_id = $(this).data('id');
        confirm("Are you sure you want to delete?");
      
       $.ajax({
            type: "DELETE",
            url: "{{ route('schools.store') }}"+'/'+school_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
     
  });
</script>
    

    <script>
        $('#male_students').on('input',function(){
            var subtotal = 0;
            $('#male_students').each(function(val){
                var currentVal;
                subtotal+=parseInt($(this).val());
            });
            studentTotal();
        })  

        $('#female_students').on('input',function(){
            var subtotal = 0;
            $('#female_students').each(function(val){
                var currentVal;
                subtotal+=parseInt($(this).val());
            });
            studentTotal();
        })

        function studentTotal(){
            var male = parseInt($('#male_students').val());
            var female = parseInt($('#female_students').val());
            $('#total_students').val(male+female);
        }
    </script>

    <script>
        $('#regular_faculty').on('input',function(){
            var subtotal = 0;
            $('#regular_faculty').each(function(val){
                var currentVal;
                subtotal+=parseInt($(this).val());
            });
            facultyTotal();
        })  

        $('#parttime_faculty').on('input',function(){
            var subtotal = 0;
            $('#parttime_faculty').each(function(val){
                var currentVal;
                subtotal+=parseInt($(this).val());
            });
            facultyTotal();
        })

        function facultyTotal(){
            var regular = parseInt($('#regular_faculty').val());
            var parttime = parseInt($('#parttime_faculty').val());
            $('#total_faculty').val(regular+parttime);
        }
    </script>


</body>
</html>